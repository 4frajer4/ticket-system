RD PROD-BUILD /S /Q
MD PROD-BUILD
call mvn clean package 
XCOPY target\lib PROD-BUILD\lib\ /e /h
XCOPY target\moneta.ticket.jar PROD-BUILD
XCOPY src\main\resources\conf\ticketsystem.properties PROD-BUILD