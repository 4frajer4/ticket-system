package cz.moneta.lesko.ticketsystem.service.test;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cz.moneta.lesko.ticketsystem.dto.Ticket;
import cz.moneta.lesko.ticketsystem.service.impl.TicketServiceImpl;
@ContextConfiguration("classpath*:conf/applicationContext-*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class TicketTest {
	@Inject
	private TicketServiceImpl ticketService;
	private Ticket ticket;
	
	@Before
	public void setupData() {
		 ticket=ticketService.generate();
	}
	@After
	public void tearDownData() {
		ticket=null;
		ticketService.getTicketList().clear();
	}
	@Test
	public void testGenerate() {
		Assert.assertEquals(Integer.valueOf(0),ticket.getPosition());
		Assert.assertEquals(Long.valueOf(1l),ticket.getId());
		Assert.assertEquals(Integer.valueOf(1),Integer.valueOf(ticketService.getTicketList().size()));
	}
	@Test
	public void testActualTicket() {
		Ticket actualTicket=ticketService.actualTicket();
		Assert.assertEquals(Integer.valueOf(actualTicket.getPosition()),Integer.valueOf(ticketService.getTicketList().get(0).getPosition()));
		Assert.assertEquals(Long.valueOf(actualTicket.getId()),Long.valueOf(ticketService.getTicketList().get(0).getId()));
	}
	@Test
	public void testRemoveLast() {
		Ticket removedTicket=ticketService.removeLast();
		Assert.assertEquals(Integer.valueOf(removedTicket.getPosition()),Integer.valueOf(0));
		ticketService.getTicketList();
		Assert.assertEquals(ticketService.getTicketList().size(),0);

	}

}
