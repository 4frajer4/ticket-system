package cz.moneta.lesko.ticketsystem.service.impl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import cz.moneta.lesko.ticketsystem.dto.Ticket;
import cz.moneta.lesko.ticketsystem.service.TicketService;
import cz.moneta.lesko.ticketsystem.utils.ProjectUtils;
import lombok.Getter;
import lombok.Setter;

@Service

public class TicketServiceImpl implements TicketService {
	@Getter
	@Setter
	private Long idCounter = 1l;
	@Getter
	private List<Ticket> ticketList = new ArrayList();

	@Inject
	private ProjectUtils projectUtils;

	public Ticket generate() {
		Ticket ticket = new Ticket();
		ticket.setId(getIdCounter());
		this.setIdCounter(getIdCounter() + 1);
		ticket.setCreationTime(projectUtils.getActualFormattedDateTime());
		ticketList.add(ticket);
		ticket.setPosition(ticketList.size() - 1);
		return ticket;
	}

	public Ticket actualTicket() {
		if (ticketList.size() > 0) {
			return ticketList.get(0);
		} else {
			return null;
		}
	}

	public Ticket removeLast() {
		if (ticketList.size() > 0) {
			Ticket ticket = ticketList.get(ticketList.size() - 1);
			ticketList.remove(ticketList.size() - 1);
			
			return ticket;
		} else {
			return null;
		}
	}

}
