package cz.moneta.lesko.ticketsystem.service;

import cz.moneta.lesko.ticketsystem.dto.Ticket;

public interface TicketService {

	public Ticket generate();
	public Ticket actualTicket();
	public Ticket removeLast();
}
