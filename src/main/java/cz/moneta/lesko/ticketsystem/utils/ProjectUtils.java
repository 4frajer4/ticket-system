package cz.moneta.lesko.ticketsystem.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ProjectUtils {

	public String getActualFormattedDateTime() {
		LocalDateTime actualTime = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		return actualTime.format(formatter);

	}
}
