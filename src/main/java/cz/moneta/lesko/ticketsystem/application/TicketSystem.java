package cz.moneta.lesko.ticketsystem.application;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.cxf.transport.servlet.CXFServlet;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.XmlWebApplicationContext;

public class TicketSystem 
{
    public static void main( String[] args )
    {
    	Properties serverProperties=initConfiguration(args);
    	initServer(serverProperties);
    }
    private static void  initServer(Properties serverProperties) {
    	Server server = new Server(Integer.valueOf(serverProperties.getProperty("ticketserver.rest.http.port")));
    	final ServletHolder servletHolder = new ServletHolder( new CXFServlet() );
        final ServletContextHandler context = new ServletContextHandler();   
        context.setContextPath( "/" );
        context.addServlet( servletHolder, "/*" );  
        context.addEventListener( new ContextLoaderListener() );

        context.setInitParameter( "contextClass", XmlWebApplicationContext.class.getName() );
        context.setInitParameter( "contextConfigLocation", "classpath*:conf/applicationContext-*.xml" );

        server.setHandler( context );
        try {
			server.start();
			server.join(); 
		} catch (Exception e) {
			System.err.println("Problem with server startup");
		}
        
        
    }
    private static Properties initConfiguration(String[] args) {
    	String configurationPath = args[0];
		File configurationFile = new File(configurationPath);
		Properties configuration = new Properties();
		try {
			configuration.load(new FileInputStream(configurationFile));
		} catch (IOException ex) {
			System.err.println("Error ocurred while reading configuration '" + configurationPath + "'.");
		}
		return configuration;
	}
    
}
