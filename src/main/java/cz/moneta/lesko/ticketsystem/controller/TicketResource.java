package cz.moneta.lesko.ticketsystem.controller;

import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Controller;

import cz.moneta.lesko.ticketsystem.dto.Ticket;
import cz.moneta.lesko.ticketsystem.service.TicketService;

@Controller
@Path("/ticket")
@Produces(MediaType.APPLICATION_JSON)
public class TicketResource {
	@Inject
	private TicketService ticketService;
	
	@POST
	@Path("generate")
	public Ticket generate() {
		return ticketService.generate();
	}
	@GET
	@Path("actual")
	public Ticket getActualTicket() {
		return ticketService.actualTicket();
	}
	@DELETE
	@Path("remove")
	public Ticket removeLastElement() {
		return ticketService.removeLast();
	}

}
