package cz.moneta.lesko.ticketsystem.dto;

import java.time.LocalDateTime;

import org.codehaus.jackson.map.annotate.JsonRootName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonRootName(value="Ticket")
public class Ticket extends AbstractDto{
	
	private String creationTime;
	private Integer position;

}
