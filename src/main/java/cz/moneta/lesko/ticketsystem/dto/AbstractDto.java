package cz.moneta.lesko.ticketsystem.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class AbstractDto {
	protected Long id;
	
}
