rm -rf PROD-BUILD
mkdir PROD-BUILD
mvn clean package
cp target/lib/ PROD-BUILD/lib/ -r
cp target/moneta.ticket.jar PROD-BUILD 
cp src/main/resources/conf/ticketsystem.properties PROD-BUILD