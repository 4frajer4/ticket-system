How to Run:
1. Run via IDE
2. Use preddefined Prod build scripts prod-build-script.bat for windows, prod-build-script.sh for Unix:
2.1. These scripts would create folder PROD-BUILD with lib folder, executable jar called moneta.ticker.jar and property file ticketsystem.properties
2.2. Run generated jar using command java -jar moneta.ticket.jar PathToPropertyFile e.g java -jar moneta.ticket.jar ticketsystem.properties
3. Manually create Jar file doing these steps:
3.1. mvn clean package - will create target folder with executable jar called moneta.ticker.jar and lib folder with dependancies
3.2. go to target folder and run executable jar with same command as mention in step 3.2